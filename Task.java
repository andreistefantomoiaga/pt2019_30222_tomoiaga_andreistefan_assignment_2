
public class Task {


    private int arrivalTime;

    private int processingTime;

    private int finishTime;

    private int id;


    public Task() {


    }


    public Task(int arrivalTime, int processingTime, int randomId) {

        this.arrivalTime = arrivalTime;

        this.processingTime = processingTime;

        id = randomId;

    }

    // finishTime =  arrivalTime + processingTime + waitingTimeOnChonenServer


    public void setFinishTime(int finishTime) {

        this.finishTime = finishTime;

    }


    public int getArrivalTime() {

        return arrivalTime;

    }


    public int getProcessingTime() {

        return processingTime;

    }


    public void setId(int id) {

        this.id = id;

    }


    public int getId() {

        return id;

    }

}