import java.util.ArrayList;

import java.util.List;

import java.util.Random;

import java.util.concurrent.BlockingQueue;


public class SimulationManager implements Runnable {


    //data from UI

    public int simulationTime;
    public int maxProcessingTime;
    public int minProcessingTime;
    public int numberOfSevers ;
    public int numberOfClients;
    public int minArrivalTime=1;
    public int maxArrivalTime=4;


    private Scheduler scheduler;


    private SimulatorFrame frame;


    private List<Task> generatedTasks;


    public SimulationManager() {

        //frame = new SimulatorFrame();

        frame = new SimulatorFrame();

    }


    private Task generateRandomTask(int arrivalTime) {


        //generate a random between maxprocessing and minprocessing

        Random r = new Random();

        int randomProcessing = r.nextInt((maxProcessingTime - minProcessingTime) + 1) + minProcessingTime;

        int randomId = r.nextInt(100);


        Task newTask = new Task(arrivalTime, randomProcessing, randomId);

        return newTask;

    }

    private int generateRandomArrival(){

        Random r = new Random();

        int randomArrival = r.nextInt((maxArrivalTime - minArrivalTime) + 1) + minArrivalTime;
        return randomArrival;
    }


    public void run() {


        System.out.println("STARTED");

        scheduler = new Scheduler(numberOfSevers, 10, frame); // aici se creaza queue


        int currentTime = 0;

        int clientCounter = 0;

        int randomTemporary = this.numberOfClients;

        int generatedRandomArrival=generateRandomArrival();

        generatedTasks = new ArrayList<Task>();


        //stabileste queues output;

        frame.setDisplayPanel(numberOfSevers, scheduler.getServers());


        while (true) {

            List<Task> currentTimeClients = new ArrayList<Task>();

            if(generatedRandomArrival == 0){

                if(currentTime <= simulationTime) {

                    int randomNumberOfGeneratedClients = 0;


                    if (clientCounter < numberOfClients) {

                        Random r = new Random();

                        randomNumberOfGeneratedClients = r.nextInt(randomTemporary);

                        randomTemporary -= randomNumberOfGeneratedClients;

                        //creaza la momentul currentTime un numar aleator de clienti din intervalul [0...randomTemporary]

                        clientCounter += randomNumberOfGeneratedClients;

                    }


                    int i = 0;

                    if (randomNumberOfGeneratedClients != 0) {

                        while (i <= randomNumberOfGeneratedClients) {


                            Task newT = generateRandomTask(currentTime);

                            currentTimeClients.add(newT);

                            frame.setLogger("Client[" + newT.getId() + "] at " + currentTime + " with PT " + newT.getProcessingTime());

                            //System.out.println("Client"+i+" with processingTime:"+newT.getProcessingTime()+" arrived at "+currentTime);

                            i++;

                        }

                    }
                }
                generatedRandomArrival=generateRandomArrival();

            }else{
                generatedRandomArrival--;
            }

            System.out.println(generatedRandomArrival +" at "+ currentTime);



            //SCHEDULER- delete client from list;

            //distribuie la servere

            // intr-un loop trebuie determinat unde se distribuie taskurile create anterior


            for (int j = 0; j < currentTimeClients.size(); j++) {

                scheduler.dispatchTask(currentTimeClients.get(j));

            }

            for (Server s : scheduler.getServers()) {


                String str = tasksToString(s.getTasks());

                frame.setQueue(s.getId(), str);


            }

            //update UI frame;


            currentTime++;


            //wait 1 second;


            try {

                Thread.sleep(1000);

            } catch (InterruptedException e) {

                e.printStackTrace();

            }

            System.out.println("\n|currentTime: " + currentTime + " |");

        }


    }

    public String tasksToString(BlockingQueue<Task> arrayBlockQueue) {


        String retString = "";


        for (Task t : arrayBlockQueue) {

            retString = retString + "Client[" + t.getId() + "]\n";

        }

        return retString;

    }


    public void setSimulationData(DTO dataContainer) {


        this.minProcessingTime = dataContainer.minProcessingTime;

        this.maxProcessingTime = dataContainer.maxProcessingTime;

        this.numberOfClients = dataContainer.numberOfClients;

        this.numberOfSevers = dataContainer.numberOfSevers;

        this.simulationTime = dataContainer.simulationTime;
        this.minArrivalTime = dataContainer.minArrivalTime;
        this.maxArrivalTime = dataContainer.maxArrivalTime;

    }


    public SimulatorFrame getSimulatorFrame() {

        return frame;

    }


}

