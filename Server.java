import javax.print.attribute.standard.Severity;

import java.util.ArrayList;

import java.util.List;

import java.util.Random;

import java.util.concurrent.atomic.AtomicInteger;


class Scheduler {


    private List<Server> servers;

    private int maxNoServers;

    private int maxTasksPerServer;


    public Scheduler(int maxNoServers, int maxTasksPerServer, SimulatorFrame frame) {


        servers = new ArrayList<Server>();

        this.maxNoServers = maxNoServers;
        this.maxTasksPerServer = maxTasksPerServer;


        for (int i = 0; i < maxNoServers; i++) {

            System.out.println("i:" + i);

            Server newServer = new Server(maxTasksPerServer, frame, i); //params;

            servers.add(newServer);

            Thread t = new Thread(newServer);

            t.start();

        }

    }

    /*

     *  ataseaza task-ul la serverul dupa strategia: pune clientul la coada cu cea mai mica perioada de asteptare;

     * */

    public void dispatchTask(Task t) {


        int bestQueueIndex = pickBestQueue();

        //System.out.println("Added task at server"+bestQueueIndex);

        servers.get(bestQueueIndex).addTask(t);

    }



    /*

     *   returneaza indexul serverului cu cel mai mic waitingPeriod;

     * */

    public int pickBestQueue() {


        int minimunWaitingTime = 99;

        int index = 0;


        for (int i = 0; i < maxNoServers; i++) {


            //determina serverul cu cel mai mic waitingperiodTime;

            if (servers.get(i).getWaitingPeriod().intValue() < minimunWaitingTime) {


                minimunWaitingTime = servers.get(i).getWaitingPeriod().intValue();

                index = i;

            }

        }


        return index;

    }


    public void setMaxNoServers(int maxNoServers) {

        this.maxNoServers = maxNoServers;

    }


    public List<Server> getServers() {


        return servers;

    }


}
