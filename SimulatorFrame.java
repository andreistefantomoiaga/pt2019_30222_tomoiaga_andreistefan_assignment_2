import sun.misc.JavaLangAccess;

import sun.plugin.javascript.JSClassLoader;


import javax.swing.*;

import javax.swing.border.Border;

import javax.swing.table.DefaultTableModel;

import java.awt.*;

import java.awt.event.ActionListener;

import java.util.ArrayList;

import java.util.List;

import java.util.Random;


public class SimulatorFrame extends JFrame {


    private JPanel panel;

    private JPanel topPanel;

    private JPanel logPanel;

    private JPanel queuePanel;


    private JTextField noOfClients;

    private JTextField minService;

    private JTextField maxService;

    private JTextField minArrival;

    private JTextField maxArrival;


    private JTextField numOfQueues;

    private JTextField simulationTime;

    private JTextArea logger;

    List<JTextArea> queues;

    List<JFrame> queueFrame;


    private JButton startSimulation;

    private int WIDTH = 620, HEIGHT = 620;


    public SimulatorFrame() {


        logger = new JTextArea(1, 1);

        staticInit();

    }


    public void setDisplayPanel(int noOfQueues, List<Server> queuesFromBackend) {


        queues = new ArrayList<JTextArea>(10);

        queueFrame = new ArrayList<JFrame>(noOfQueues);


        for (int i = 0; i < noOfQueues; i++) {


            queueFrame.add(new JFrame());

            queues.add(new JTextArea());


            queues.get(i).setBounds(0, 0, 150, 200);

            queueFrame.get(i).setVisible(true);

            queueFrame.get(i).setBounds(i * 200, 0, 150, 300);

            queueFrame.get(i).add(queues.get(i));


        }


        /*queues= new ArrayList<JTable>(noOfQueues);



       for (int i=0;i<noOfQueues;i++){



            queues.add(new JTable());

            queues.get(i).setBounds(1,4,100,100);



            queuePanel.add(queues.get(i));



        }

*/


    }


    public void delete() {

        queuePanel.setVisible(false);

        //panel.remove(queuePanel);

    }


    public void staticInit() {


        setSize(WIDTH, HEIGHT);


        panel = new JPanel();

        panel.setSize(WIDTH, HEIGHT);

        panel.setBackground(Color.WHITE);


        add(panel);


        topPanel = new JPanel();

        logPanel = new JPanel();

        queuePanel = new JPanel();

        queuePanel.setBounds(0, 71, 450, 500);

        queuePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        //lets create a UI with inputfields for minimum and maximum arriving time between customers

        //        JTextField minArr = new JTextField();

        Font myFont = new Font("Dialog", 1, 10);


        noOfClients = new JTextField("20");

        minService = new JTextField("3");

        maxService = new JTextField("10");

        numOfQueues = new JTextField("3");

        minArrival  = new JTextField("2");
        maxArrival = new JTextField("4");

        simulationTime = new JTextField("50");

        startSimulation = new JButton("Start Simulation");

        JLabel logLab = new JLabel("Logger");


        JLabel numberOfClientsLab = new JLabel("No. of Clients");
        JLabel arrTimeintLab = new JLabel("Arrival Interval");
        JLabel numberOfQueuesLab = new JLabel("No. of Queues");

        JLabel processingTimeLab = new JLabel("Processing Time");
        JLabel dots = new JLabel(":");

        JLabel minLab = new JLabel("MIN");

        JLabel maxLab = new JLabel("MAX");

        JLabel simTimeLab = new JLabel("Simulation time");


        minLab.setFont(myFont);

        maxLab.setFont(myFont);


        numberOfClientsLab.setBounds(5, 40, 75, 30);

        numberOfQueuesLab.setBounds(5 + 75 + 5, 40, 80, 30);

        processingTimeLab.setBounds(90 + 80, 40, 100, 30);

        dots.setBounds(90 + 80 + 47, 10, 10, 30);

        minLab.setBounds(90 + 80 + 10, -5, 30, 20);

        maxLab.setBounds(90 + 80 + 67, -5, 30, 20);

        simTimeLab.setBounds(275, 40, 100, 30);


        logPanel.setBounds(450, 71, 150, 500);

        topPanel.setBounds(0, 0, 600, 70);


        topPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        logPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        topPanel.setLayout(null);

        logPanel.setLayout(null);


        noOfClients.setBounds(5, 10, 75, 30);

        numOfQueues.setBounds(5 + 75 + 5, 10, 80, 30);


        minService.setBounds(90 + 80, 10, 40, 30);

        maxService.setBounds(170 + 40 + 20, 10, 40, 30);


        simulationTime.setBounds(275, 10, 90, 30);


        startSimulation.setBounds(457, 15, 130, 30);

        minArrival.setBounds(275+90,10,40,30);
        maxArrival.setBounds(275+40+5+90,10,40,30);
        arrTimeintLab.setBounds(275+90,40,100,30);


        logLab.setBounds(55, 0, 100, 30);

        logger.setBounds(5, 25, 140, 520 - 30 - 20);

        logger.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        logPanel.add(logLab);

        logPanel.add(logger);


        topPanel.add(noOfClients);

        topPanel.add(minService);

        topPanel.add(maxService);

        topPanel.add(numOfQueues);
        topPanel.add(simulationTime);

        topPanel.add(startSimulation);

        topPanel.add(numberOfClientsLab);

        topPanel.add(numberOfQueuesLab);

        topPanel.add(processingTimeLab);

        topPanel.add(dots);

        topPanel.add(minLab);

        topPanel.add(maxLab);
        topPanel.add(minArrival);
        topPanel.add(maxArrival);
        topPanel.add(arrTimeintLab);

        topPanel.add(simTimeLab);

        panel.add(topPanel);

        panel.add(logPanel);

        panel.add(queuePanel);

        panel.setLayout(null);


        panel.setBackground(Color.RED);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLocationRelativeTo(null);

        setVisible(true);

        setLayout(null);

        setResizable(false);

    }


    public void init() {


        panel = new JPanel();

        panel.setSize(WIDTH, HEIGHT);

        panel.setBackground(Color.WHITE);


        this.add(panel);

        this.setSize(WIDTH, HEIGHT);


        topPanel = new JPanel();

        logPanel = new JPanel();


        //lets create a UI with inputfields for minimum and maximum arriving time between customers

        //        JTextField minArr = new JTextField();

        Font myFont = new Font("Dialog", 1, 10);


        noOfClients = new JTextField("20");

        minService = new JTextField("3");

        maxService = new JTextField("10");

        numOfQueues = new JTextField("3");

        simulationTime = new JTextField("50");

        startSimulation = new JButton("Start Simulation");

        JLabel logLab = new JLabel("Logger");


        JLabel numberOfClientsLab = new JLabel("No. of Clients");

        JLabel numberOfQueuesLab = new JLabel("No. of Queues");
        JLabel processingTimeLab = new JLabel("Processing Time");

        JLabel dots = new JLabel(":");
        JLabel minLab = new JLabel("MIN");

        JLabel maxLab = new JLabel("MAX");

        JLabel simTimeLab = new JLabel("Simulation time");


        minLab.setFont(myFont);

        maxLab.setFont(myFont);


        numberOfClientsLab.setBounds(5, 40, 75, 30);

        numberOfQueuesLab.setBounds(5 + 75 + 5, 40, 80, 30);

        processingTimeLab.setBounds(90 + 80, 40, 100, 30);

        dots.setBounds(90 + 80 + 47, 10, 10, 30);

        minLab.setBounds(90 + 80 + 10, -5, 30, 20);

        maxLab.setBounds(90 + 80 + 67, -5, 30, 20);

        simTimeLab.setBounds(275, 40, 100, 30);


        topPanel.setBounds(0, 0, 600, 70);

        topPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        logPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        topPanel.setLayout(null);

        logPanel.setLayout(null);


        logPanel.setBounds(450, 71, 150, 500);


        noOfClients.setBounds(5, 10, 75, 30);

        numOfQueues.setBounds(5 + 75 + 5, 10, 80, 30);


        minService.setBounds(90 + 80, 10, 40, 30);

        maxService.setBounds(170 + 40 + 20, 10, 40, 30);


        simulationTime.setBounds(275, 10, 90, 30);


        startSimulation.setBounds(457, 15, 130, 30);


        logLab.setBounds(55, 0, 100, 30);

        logger.setBounds(5, 25, 140, 520 - 30 - 20);

        logger.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        logPanel.add(logLab);

        logPanel.add(logger);


        topPanel.add(noOfClients);

        topPanel.add(minService);

        topPanel.add(maxService);

        topPanel.add(numOfQueues);

        topPanel.add(simulationTime);

        topPanel.add(startSimulation);
        topPanel.add(numberOfClientsLab);
        topPanel.add(minArrival);
        topPanel.add(maxArrival);
        topPanel.add(numberOfQueuesLab);

        topPanel.add(processingTimeLab);

        topPanel.add(dots);

        topPanel.add(simTimeLab);


        panel.add(topPanel);

        panel.add(logPanel);

        panel.setLayout(null);


        panel.setBackground(Color.RED);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLocationRelativeTo(null);

        setVisible(true);

        setLayout(null);

        setResizable(false);


    }


    public DTO gatherData() {


        DTO dataContainer = new DTO();


        dataContainer.maxProcessingTime = new Integer(maxService.getText()).intValue();

        dataContainer.minProcessingTime = new Integer(minService.getText()).intValue();

        dataContainer.numberOfClients = new Integer(noOfClients.getText()).intValue();

        dataContainer.numberOfSevers = new Integer(numOfQueues.getText()).intValue();

        dataContainer.simulationTime = new Integer(simulationTime.getText()).intValue();
        dataContainer.minArrivalTime =  new Integer(minArrival.getText()).intValue();
        dataContainer.maxArrivalTime =  new Integer(maxArrival.getText()).intValue();


        return dataContainer;


    }


    public JTextArea getLogger() {

        return logger;

    }

    public void setLogger(String str) {


        logger.append(str);

        logger.append("\n");

    }

    public void setQueue(int index, String str) {


        queues.get(index).setText("");

        queues.get(index).append(str);


    }


    public void addStartListener(ActionListener arg) {

        startSimulation.addActionListener(arg);

    }


    public static void main(String[] args) {

        SimulatorFrame sf = new SimulatorFrame();

    }

}