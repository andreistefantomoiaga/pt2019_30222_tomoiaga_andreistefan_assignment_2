import java.util.ArrayList;

import java.util.List;

import java.util.concurrent.ArrayBlockingQueue;

import java.util.concurrent.BlockingQueue;

import java.util.concurrent.atomic.AtomicInteger;


class Server implements Runnable {


    private BlockingQueue<Task> tasks;

    private AtomicInteger waitingPeriod;

    private SimulatorFrame frame;

    private int id;


    public Server(int maxTasksPerServer, SimulatorFrame frame, int identification) {


        tasks = new ArrayBlockingQueue<Task>(maxTasksPerServer);

        waitingPeriod = new AtomicInteger(0);

        this.frame = frame;

        this.id = identification;


    }

    public void addTask(Task newTask) {


        try {

            tasks.put(newTask);  //ADD SAU PUT YOU DECIDE!;

        } catch (InterruptedException e) {

            e.printStackTrace();

        }

        waitingPeriod.set(waitingPeriod.intValue() + newTask.getProcessingTime());

    }


    public void run() {


        int counter = 0;


        while (true) {

            if (!tasks.isEmpty()) {


                //trebuie facut sa astepte cat timp are clientul de procesat
                // trebuie sters clientul

                //trebuie decrementat waitingPeriod-ul la server


                try {

                    Task auxTask = tasks.element();

                    Thread.sleep(auxTask.getProcessingTime() * 1000);

                    decrementWaitingPeriod(auxTask.getProcessingTime());

                    tasks.take();

                    //

                    int idTask = auxTask.getId();

                    int finishTime = auxTask.getProcessingTime() + auxTask.getArrivalTime() + this.getWaitingPeriod().intValue();

                    //

                    frame.setLogger("Client[" + idTask + "] served at " + finishTime);


                } catch (InterruptedException e) {

                    e.printStackTrace();

                }


            }


        }

    }


    public int getId() {

        return id;

    }


    public BlockingQueue<Task> getTasks() {

        return tasks;

    }


    public void decrementWaitingPeriod(int time) {

        waitingPeriod.set(waitingPeriod.intValue() - time);
    }


    public Integer getWaitingPeriod() {

        return waitingPeriod.intValue();

    }


    public void setWaitingPeriod(AtomicInteger waitingPeriod) {

        this.waitingPeriod = waitingPeriod;

    }

}