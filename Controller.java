import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller {

    private SimulationManager simMan;

    private SimulatorFrame simFrame;


    public Controller(SimulationManager sM, SimulatorFrame sF) {

        simMan = sM;
        simFrame = sF;

        simFrame.addStartListener(new startSimulationListener());


    }


    private class startSimulationListener implements ActionListener {


        public void actionPerformed(ActionEvent e) {


            System.out.println("PRESSED");


            DTO dataContainer;


            dataContainer = simFrame.gatherData();

            simMan.setSimulationData(dataContainer);

            Thread mainThread = new Thread(simMan);

            mainThread.start();

        }

    }


    public static void main(String[] args) {


        SimulationManager mySim = new SimulationManager();

        Controller myCtrl = new Controller(mySim, mySim.getSimulatorFrame());


    }


}