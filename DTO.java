public class DTO {


    public int simulationTime;

    public int maxProcessingTime;

    public int minProcessingTime;

    public int numberOfSevers;

    public int numberOfClients;
    public int minArrivalTime;
    public int maxArrivalTime;



    public DTO() {


        simulationTime = 0;

        maxProcessingTime = 0;

        minProcessingTime = 0;

        numberOfSevers = 0;

        numberOfClients = 0;
        minArrivalTime=0;
                maxArrivalTime=0;

    }

}